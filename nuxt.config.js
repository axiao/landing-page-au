export default {
  // ssr: false,
  // Target: https://go.nuxtjs.dev/config-target
  // target: 'static',

  // Global page headers: https://go.nuxtjs.dev/config-head
  head: {
    title: 'Performance creative platform',
    meta: [
      { charset: 'utf-8' },
      { name: 'viewport', content: 'width=device-width, initial-scale=1' },
      { hid: 'description', name: 'description', content: '' },
      { name: 'format-detection', content: 'telephone=no' }
    ],
    link: [
      { rel: 'icon', type: 'image/x-icon', href: '/favicon.ico' }
    ]
  },

  // Global CSS: https://go.nuxtjs.dev/config-css
  css: [
    '@/assets/scss/style.scss'
  ],

  // Plugins to run before rendering page: https://go.nuxtjs.dev/config-plugins
  plugins: [
  ],

  // Auto import components: https://go.nuxtjs.dev/config-components
  components: true,

  // Modules for dev and build (recommended): https://go.nuxtjs.dev/config-modules
  buildModules: [
    // https://go.nuxtjs.dev/eslint
    '@nuxtjs/eslint-module',
  ],

  publicRuntimeConfig: {
    gtm: {
      id: 'GTM-KKLC638'
    }
  },

  // Modules: https://go.nuxtjs.dev/config-modules
  modules: [
    // https://go.nuxtjs.dev/bootstrap
    'bootstrap-vue/nuxt',
    // https://go.nuxtjs.dev/axios
    '@nuxtjs/axios',
    // https://go.nuxtjs.dev/pwa
    '@nuxtjs/pwa',

    '@nuxtjs/firebase',
    '@nuxtjs/gtm',

    // 'vue-read-more',
  ],

  gtm: {
    id: 'GTM-KKLC638'
  },

  firebase: {
    config: {
      apiKey: "AIzaSyA2euNoQDkCpLaQqzhVkAg94Ww0Dw8TyL4",
      authDomain: "tiktok-video-46036.firebaseapp.com",
      projectId: "tiktok-video-46036",
      storageBucket: "tiktok-video-46036.appspot.com",
      messagingSenderId: "178724630660",
      appId: "1:178724630660:web:76e1fd3f9a2e86521052d1",
      measurementId: "G-QRJ0E1B8GY"
    },
    services: {
      auth: {
        initialize: {
          onAuthStateChangedAction: 'user/onAuthStateChanged',
        },
        ssr: true,
      },
      firestore: true,
    }
  },


  // Axios module configuration: https://go.nuxtjs.dev/config-axios
  axios: {
    // Workaround to avoid enforcing hard-coded localhost:3000: https://github.com/nuxt-community/axios-module/issues/308
    baseURL: '/',
  },

  // PWA module configuration: https://go.nuxtjs.dev/pwa
  pwa: {
    manifest: {
      lang: 'en'
    }
  },

  // Build Configuration: https://go.nuxtjs.dev/config-build
  build: {
    babel: {
      compact: true,
     },
  }
}
